use glium::{glutin::{self, dpi::PhysicalPosition}, Surface};
use crate::board::{ChessBoard, BoardPosition};

const SCREEN_WIDTH: f32 = 480.0;
const SCREEN_HEIGHT: f32 = 480.0;

// Vertex definition
#[derive(Copy, Clone)]
pub struct Vertex {
    position: [f32; 2],
    tex_coords: [f32; 2],
}

glium::implement_vertex!(Vertex, position, tex_coords);

// Vertices
pub const TILE_VERTICES: [Vertex; 4] = [
    Vertex { position: [-1.0, -1.0 ], tex_coords: [0.0, 0.0] }, // top left
    Vertex { position: [-1.0, 1.0 ], tex_coords: [0.0, 1.0] }, // bottom left
    Vertex { position: [1.0, 1.0 ], tex_coords: [1.0, 1.0] }, // bottom right
    Vertex { position: [1.0, -1.0 ], tex_coords: [1.0, 0.0] }, // top right
];

// OpenGL shaders
pub const VERTEX_SHADER: &'static str = r#"
#version 140

uniform mat4 matrix;
uniform vec2 translate;
uniform vec4 colour;

in vec2 position;
in vec2 tex_coords;

out vec2 v_tex_coords;
out vec4 passed_colour;

void main() {
    vec2 translated = position + translate;
    gl_Position = matrix * vec4(translated, 0.0, 1.0);
    v_tex_coords = tex_coords;
    passed_colour = colour;
}
"#;

pub const FRAGMENT_SHADER: &'static str = r#"
#version 140

uniform sampler2D tex;

in vec2 v_tex_coords;
in vec4 passed_colour;

out vec4 f_color;

void main() {
    f_color = texture(tex, v_tex_coords) * passed_colour;
}
"#;

// Texture loading
fn load_texture(display: &glium::Display, path: &'static str) -> glium::Texture2d {
    let img = image::io::Reader::open(path).unwrap().decode().unwrap().to_rgba8();

    let img_dim = img.dimensions();
    let raw_img = glium::texture::RawImage2d::from_raw_rgba_reversed(&img.into_raw(), img_dim);

    glium::texture::Texture2d::new(display, raw_img).unwrap()
}

pub fn load_all_textures(display: &glium::Display) -> [glium::Texture2d; 14] {
    [
        load_texture(&display, "./assets/wht_king.png"),  // 00  White King
        load_texture(&display, "./assets/wht_queen.png"), // 01  White Queen
        load_texture(&display, "./assets/wht_rook.png"),  // 02  White Rook
        load_texture(&display, "./assets/wht_bishop.png"),// 03  White Bishop
        load_texture(&display, "./assets/wht_knight.png"),// 04  White Knight
        load_texture(&display, "./assets/wht_pawn.png"),  // 05  White Pawn
        load_texture(&display, "./assets/blk_king.png"),  // 06  Black King
        load_texture(&display, "./assets/blk_queen.png"), // 07  Black Queen
        load_texture(&display, "./assets/blk_rook.png"),  // 08  Black Rook
        load_texture(&display, "./assets/blk_bishop.png"),// 09  Black Bishop
        load_texture(&display, "./assets/blk_knight.png"),// 10  Black Knight
        load_texture(&display, "./assets/blk_pawn.png"),  // 11  Black Pawn
        load_texture(&display, "./assets/px.png"),        // 12  White pixel (for board pieces)
        load_texture(&display, "./assets/valid_move.png"),// 13  Dot for valid move
    ]
}

// Display
pub struct ChessDisplay {
    display: glium::Display,
    program: glium::Program,
    piece_vertex_buffer: glium::VertexBuffer<Vertex>,
    piece_index_buffer: glium::IndexBuffer<u16>,
    piece_textures: [glium::Texture2d; 14],
    draw_parameters: glium::DrawParameters<'static>,
}

impl ChessDisplay {
    pub fn new<T: 'static>(event_loop: &glutin::event_loop::EventLoop<T>) -> Self {
        let wb = glutin::window::WindowBuilder::new()
            .with_inner_size(glutin::dpi::LogicalSize::new(SCREEN_WIDTH, SCREEN_HEIGHT))
            .with_title("Hello world!")
            .with_resizable(false);
        let cb = glutin::ContextBuilder::new().with_vsync(true);
        let glium_display = glium::backend::glutin::Display::new(wb, cb, event_loop).unwrap();
        Self {
            program: glium::Program::from_source(
                &glium_display,
                VERTEX_SHADER,
                FRAGMENT_SHADER,
                None
            ).unwrap(),
            piece_vertex_buffer: glium::VertexBuffer::new(
                &glium_display, 
                &TILE_VERTICES
            ).unwrap(),
            piece_index_buffer: glium::IndexBuffer::new(
                &glium_display,
                glium::index::PrimitiveType::TriangleStrip,
                &[1 as u16, 2, 0, 3]
            ).unwrap(),
            piece_textures: load_all_textures(&glium_display),
            draw_parameters: glium::draw_parameters::DrawParameters {
                blend: glium::draw_parameters::Blend::alpha_blending(),
                ..Default::default()
            },
            display: glium_display,
        }
    }

    pub fn draw(&self, board: &ChessBoard) {
        // Enclosure for drawing a chess piece, given a texture and a board position
        let draw_piece = |target_frame: &mut glium::Frame, texture: &glium::Texture2d, tile: crate::board::BoardPosition| {
            let translation = [tile.file as f32 * 2.0 - 7.0, 7.0 - (tile.rank as f32 * 2.0)];
            let uniform = glium::uniform! {
                matrix: [
                    [0.125, 0.0, 0.0, 0.0],
                    [0.0, 0.125, 0.0, 0.0],
                    [0.0, 0.0, 1.0, 0.0],
                    [0.0, 0.0, 0.0, 1.0f32],
                ],
                tex: texture,
                translate: translation,
                colour: [1.0, 1.0, 1.0, 1.0f32]
            };
            target_frame.draw(&self.piece_vertex_buffer, &self.piece_index_buffer, &self.program, &uniform, &self.draw_parameters).unwrap();
        };

        // Enclosure for drawing the chessboard itself, tile by tile. Allows a tile colour (eg. to highlight a tile)
        let draw_tile = |target_frame: &mut glium::Frame, tile_colour: [f32; 4], tile: BoardPosition| {
            let translation = [tile.file as f32 * 2.0 - 7.0, 7.0 - (tile.rank as f32 * 2.0)];
            target_frame.draw(&self.piece_vertex_buffer, &self.piece_index_buffer, &self.program, &glium::uniform! {
                matrix: [
                    [0.125, 0.0, 0.0, 0.0],
                    [0.0, 0.125, 0.0, 0.0],
                    [0.0, 0.0, 1.0, 0.0],
                    [0.0, 0.0, 0.0, 1.0f32],
                ],
                tex: &self.piece_textures[12],
                translate: translation,
                colour: tile_colour,
            }, &self.draw_parameters).unwrap();
        };

        let draw_valid_move = |target_frame: &mut glium::Frame, tile: BoardPosition| {
            let translation = [tile.file as f32 * 2.0 - 7.0, 7.0 - (tile.rank as f32 * 2.0)];
            target_frame.draw(&self.piece_vertex_buffer, &self.piece_index_buffer, &self.program, &glium::uniform! {
                matrix: [
                    [0.125, 0.0, 0.0, 0.0],
                    [0.0, 0.125, 0.0, 0.0],
                    [0.0, 0.0, 1.0, 0.0],
                    [0.0, 0.0, 0.0, 1.0f32],
                ],
                tex: &self.piece_textures[13],
                translate: translation,
                colour: [0.0, 1.0, 0.0, 0.5f32],
            }, &self.draw_parameters).unwrap();
        };
    
        // Translate a board::BoardPiece into a Texture2d
        let get_texture = |chess_piece: crate::board::BoardPiece| {
            &self.piece_textures[chess_piece.texture_index()]
        };
    
        let mut target = self.display.draw();
        target.clear_color(1.0, 1.0, 1.0, 1.0);

        // Draw the board...
        for i in 0..8usize {
            for j in 0..8usize {
                if (i % 2 == 1) ^ (j % 2 == 1) {
                    draw_tile(&mut target, [128.0 / 255.0, 96.0 / 255.0, 42.0 / 255.0, 1.0f32], crate::board::BoardPosition{ rank: j, file: i });
                }
            }
        }

        // ... then the highlighted square ...
        if let Some(hilited_square) = board.selected_position {
            draw_tile(&mut target, [131.0 / 255.0, 189.0 / 255.0, 119.0 / 255.0, 1.0f32], hilited_square);
        }

        // ... then all the pieces ...
        for i in 0..8usize {
            for j in 0..8usize {
                let board_position = crate::board::BoardPosition { rank: j, file: i };
                if let Some(piece) = board.tile(board_position) {
                    draw_piece(
                        &mut target,
                        get_texture(piece),
                        board_position
                    );
                };
            }
        }

        // ... then the valid moves ...
        if let Some(selected_pos) = board.selected_position {
            let valid_moves = &board.valid_moves[&selected_pos];
            for pos in valid_moves {
                draw_valid_move(&mut target, *pos);
            }
        }
        target.finish().unwrap();
    }

    pub fn get_tile_from_pixels(position: PhysicalPosition<f64>) -> crate::board::BoardPosition {
        const TILE_SIZE_IN_PIXELS: f64 = SCREEN_WIDTH as f64 / 8.0;
        let x: f64 = position.x / TILE_SIZE_IN_PIXELS;
        let y: f64 = position.y / TILE_SIZE_IN_PIXELS;
        crate::board::BoardPosition{ rank: y as usize, file: x as usize }
    }
}