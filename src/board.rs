use std::{fmt, collections::HashMap};

#[derive(Clone, Copy, Debug)]
enum PieceColour {
    White,
    Black,
}

impl PieceColour {
    fn texture_offset(&self) -> usize {
        match self {
            PieceColour::White => 0,
            PieceColour::Black => 6,
        }
    }
}

#[derive(Clone, Copy, Debug)]
enum ChessPiece {
    King,
    Queen,
    Rook,
    Bishop,
    Knight,
    Pawn,
}

use ChessPiece::*;

impl ChessPiece {
    fn texture_index(&self) -> usize {
        match self {
            ChessPiece::King => 0,
            ChessPiece::Queen => 1,
            ChessPiece::Rook => 2,
            ChessPiece::Bishop => 3,
            ChessPiece::Knight => 4,
            ChessPiece::Pawn => 5,
        }
    }
}

const DEFAULT_PIECE_ORDER: [ChessPiece; 8] = [Rook, Knight, Bishop, Queen, King, Bishop, Knight, Rook];

#[derive(Clone, Copy, Debug, Hash, PartialEq, Eq)]
pub struct BoardPosition {
    pub rank: usize, // Rows
    pub file: usize, // Columns
}

impl BoardPosition {
    fn new(rank: usize, file: usize) -> Self {
        assert!(rank <= 7 && file <= 7);
        Self {
            rank: rank,
            file: file,
        }
    }

    fn left(&self) -> Option<Self> {
        if self.file == 0 {
            None
        } else {
            Some(Self::new(self.rank, self.file - 1))
        }
    }

    fn right(&self) -> Option<Self> {
        if self.file == 7 {
            None
        } else {
            Some(Self::new(self.rank, self.file + 1))
        }
    }

    fn up(&self) -> Option<Self> {
        if self.rank == 0 {
            None
        } else {
            Some(Self::new(self.rank - 1, self.file))
        }
    }

    fn down(&self) -> Option<Self> {
        if self.rank == 7 {
            None
        } else {
            Some(Self::new(self.rank + 1, self.file))
        }
    }

    fn up_left(&self) -> Option<Self> {
        if let Some(position) = self.up() {
            position.left()
        } else {
            None
        }
    }

    fn up_right(&self) -> Option<Self> {
        if let Some(position) = self.up() {
            position.right()
        } else {
            None
        }
    }

    fn down_left(&self) -> Option<Self> {
        if let Some(position) = self.down() {
            position.left()
        } else {
            None
        }
    }

    fn down_right(&self) -> Option<Self> {
        if let Some(position) = self.down() {
            position.right()
        } else {
            None
        }
    }

    fn change_rank(&self, offset: usize, down: bool) -> Option<Self> {
        if down {
            if self.rank + offset > 7 {
                return None
            }
            Some(Self::new(self.rank + offset, self.file))
        } else {
            if self.rank < offset {
                return None
            }
            Some(Self::new(self.rank - offset, self.file))
        }
    }

    fn change_file(&self, offset: usize, right: bool) -> Option<Self> {
        if right {
            if self.file + offset > 7 {
                return None
            }
            Some(Self::new(self.rank, self.file + offset))
        } else {
            if self.file < offset {
                return None
            }
            Some(Self::new(self.rank, self.file - offset))
        }
    }

    fn rank_as_algebraic(&self) -> usize {
        8 - self.rank
    }

    fn file_as_algebraic(&self) -> char {
        (('a' as u8) + self.file as u8) as char
    }
}

impl fmt::Display for BoardPosition {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let rank = self.rank_as_algebraic();
        let file = self.file_as_algebraic();
        write!(f, "{file}{rank}")
    }
}

// The actual pieces on the board.
#[derive(Clone, Copy, Debug)]
pub struct BoardPiece {
    piece_type: ChessPiece,
    piece_colour: PieceColour,
}

impl BoardPiece {
    pub fn texture_index(&self) -> usize {
        self.piece_type.texture_index() + self.piece_colour.texture_offset()
    }

    fn rook_moves(&self, board: &ChessBoard, position: BoardPosition) -> Vec<BoardPosition> {
        let mut valid_positions = board.direction_until_piece(position, &BoardPosition::up);
        valid_positions.append(&mut board.direction_until_piece(position, &BoardPosition::down));
        valid_positions.append(&mut board.direction_until_piece(position, &BoardPosition::left));
        valid_positions.append(&mut board.direction_until_piece(position, &BoardPosition::right));
        valid_positions
    }

    fn bishop_moves(&self, board: &ChessBoard, position: BoardPosition) -> Vec<BoardPosition> {
        let mut valid_positions = board.direction_until_piece(position, &BoardPosition::up_left);
        valid_positions.append(&mut board.direction_until_piece(position, &BoardPosition::up_right));
        valid_positions.append(&mut board.direction_until_piece(position, &BoardPosition::down_left));
        valid_positions.append(&mut board.direction_until_piece(position, &BoardPosition::down_right));
        valid_positions
    }

    pub fn get_valid_moves(&self, board: &ChessBoard, position: BoardPosition) -> Vec<BoardPosition> {
        // TODO: Captures. Each piece kind of does a random thing when a piece is on its target square.
        // TODO: Pins.
        match self.piece_type {
            King => {
                // TODO: Make sure king doesn't get into check!
                let positions = vec![
                    position.up(),
                    position.up_right(),
                    position.right(),
                    position.down_right(),
                    position.down(),
                    position.down_left(),
                    position.left(),
                    position.up_left(),
                ];
                positions.into_iter().flatten().collect()
            },
            Queen => {
                let mut positions = self.rook_moves(board, position);
                positions.append(&mut self.bishop_moves(board, position));
                positions
            },
            Rook => self.rook_moves(board, position),
            Bishop => self.bishop_moves(board, position),
            Knight => {
                let mut positions: Vec<Option<BoardPosition>> = vec![];
                if let Some(pos) = position.change_rank(2, false) {
                    positions.push(pos.left());
                    positions.push(pos.right());
                }
                if let Some(pos) = position.change_rank(2, true) {
                    positions.push(pos.left());
                    positions.push(pos.right());
                }
                if let Some(pos) = position.change_file(2, false) {
                    positions.push(pos.up());
                    positions.push(pos.down());
                }
                if let Some(pos) = position.change_file(2, true) {
                    positions.push(pos.up());
                    positions.push(pos.down());
                }
                positions.into_iter().flatten().collect()
            },
            Pawn => {
                let get_pawn_moves = |down: bool, starting_rank: usize| -> Vec<BoardPosition> {
                    let mut valid_positions: Vec<BoardPosition> = vec![];
                    let position_1 = position.change_rank(1, down).expect("Pawn should never be able to move beyond the end of the board.");
                    if let None = board.tile(position_1) {
                        valid_positions.push(position_1);
                        if position.rank == starting_rank {
                            let position_2 = position_1.change_rank(1, down).expect("Pawn should never be able to move beyond the end of the board.");
                            if let None = board.tile(position_2) {
                                valid_positions.push(position_2);
                            }
                        }
                    }
                    valid_positions
                };
                match self.piece_colour {
                    PieceColour::White => {
                        get_pawn_moves(false, 6)
                    },
                    PieceColour::Black => {
                        get_pawn_moves(true, 1)
                    },
                }
            },
        }
    }
}

pub struct ChessBoard {
    board: [[Option<BoardPiece>; 8]; 8],
    pub valid_moves: HashMap<BoardPosition, Vec<BoardPosition>>,
    pub selected_position: Option<BoardPosition>,
}

impl ChessBoard {
    pub fn new() -> Self {
        let board_array: [[Option<BoardPiece>; 8]; 8] = {
            let mut board_array = [[None; 8]; 8];
            board_array[1] = [Some(BoardPiece { piece_type: Pawn, piece_colour: PieceColour::Black }); 8];
            board_array[6] = [Some(BoardPiece { piece_type: Pawn, piece_colour: PieceColour::White }); 8];
            for i in 0..8 {
                board_array[0][i] = Some(BoardPiece {
                    piece_type: DEFAULT_PIECE_ORDER[i],
                    piece_colour: PieceColour::Black,
                });
                board_array[7][i] = Some(BoardPiece {
                    piece_type: DEFAULT_PIECE_ORDER[i],
                    piece_colour: PieceColour::White,
                });
            }

            board_array[5][5] = Some(BoardPiece {
                piece_type: Queen,
                piece_colour: PieceColour::White,
            });
            board_array
        };
        let mut new_board = Self {
            board: board_array,
            valid_moves: HashMap::new(),
            selected_position: None,
        };
        new_board.recalculate_valid_moves();
        new_board
    }

    pub fn tile(&self, position: BoardPosition) -> Option<BoardPiece> {
        self.tile_from_rank_file(position.rank, position.file)
    }

    pub fn tile_from_rank_file(&self, rank: usize, file: usize) -> Option<BoardPiece> {
        self.board[rank][file]
    }

    pub fn update_selected_position(&mut self, position: BoardPosition) {
        // Only selects a position if there is no piece on that square.
        if let Some(_) = self.tile(position) {
            self.selected_position = Some(position);
        } else {
            self.selected_position = None;
        }
    }

    pub fn recalculate_valid_moves(&mut self) {
        for i in 0..8 {
            for j in 0..8 {
                let position = BoardPosition::new(i, j);
                if let Some(board_piece) = self.tile(position) {
                    self.valid_moves.insert(position, board_piece.get_valid_moves(self, position));
                }
            }
        }
    }

    fn direction_until_piece(&self, position: BoardPosition, direction_func: &dyn Fn(&BoardPosition) -> Option<BoardPosition>) -> Vec<BoardPosition> {
        let mut valid_moves: Vec<BoardPosition> = vec![];
        let mut pos = position.clone();
        loop {
            // We're not at the edge of the board
            if let Some(new_pos) = direction_func(&pos) {
                pos = new_pos;
            } else {
                return valid_moves
            }

            // Is there a piece there?
            if let Some(_) = self.tile(pos) {
                return valid_moves
            } else {
                valid_moves.push(pos)
            }
        }
    }
}