extern crate glium;
extern crate image;

mod renderer;
mod board;

use glium::{glutin::{self, dpi::PhysicalPosition}};

fn main() {
    // Window initialization
    let event_loop = glutin::event_loop::EventLoop::new();
    let chess_display = renderer::ChessDisplay::new(&event_loop);

    let mut board: board::ChessBoard = board::ChessBoard::new();

    let mut last_frame_time: std::time::Instant = std::time::Instant::now();
    //let mut last_update_time: std::time::Instant = last_frame_time.clone();
    /*
    let mut frames_this_second: u32 = 0;
    let mut last_second: std::time::Instant = last_frame_time.clone();
    */
    let mut pressed: bool = false;
    let mut cursor_position: PhysicalPosition<f64> = PhysicalPosition { x: 0.0, y: 0.0 };
    event_loop.run(move |event, _, control_flow| {
        *control_flow = match event {
            glutin::event::Event::WindowEvent { event, .. } => match event {
                glutin::event::WindowEvent::CloseRequested => glutin::event_loop::ControlFlow::Exit,
                glutin::event::WindowEvent::Resized(..) => {
                    chess_display.draw(&board);
                    glutin::event_loop::ControlFlow::Poll
                },
                glutin::event::WindowEvent::CursorMoved { position, .. } => {
                    cursor_position = position;
                    glutin::event_loop::ControlFlow::Poll
                }
                glutin::event::WindowEvent::MouseInput { state, button, .. } => {
                    if button == glutin::event::MouseButton::Left && state == glutin::event::ElementState::Pressed {
                        pressed = true;
                    }
                    glutin::event_loop::ControlFlow::Poll
                }
                _ => glutin::event_loop::ControlFlow::Poll,
            },
            glutin::event::Event::MainEventsCleared => {
                // Update events (animations, etc.)
                //let time_now = std::time::Instant::now();
                //let micros_since_update: u128 = (time_now - last_update_time).as_micros();
                // do update stuff
                //last_update_time = time_now;
                if pressed == true {
                    pressed = false;
                    let selected_position: board::BoardPosition = renderer::ChessDisplay::get_tile_from_pixels(cursor_position);
                    board.update_selected_position(selected_position);
                }
                // Update drawing frames.
                if (std::time::Instant::now() - last_frame_time).as_nanos() > 16_666_666 {
                    last_frame_time = std::time::Instant::now();
                    // Display FPS
                    /*
                    frames_this_second += 1;
                    if (last_frame_time - last_second).as_secs_f32() >= 1.0 {
                        println!("{} FPS", frames_this_second);
                        frames_this_second = 0;
                        last_second = last_frame_time.clone();
                    }
                    */
                    chess_display.draw(&board);
                }
                glutin::event_loop::ControlFlow::Poll
            },
            _ => glutin::event_loop::ControlFlow::Poll,
        };
    });
}
